/*
 * Copyright (c) <year> <copyright holders>
 *
 * Anti 996 License Version 1.0 (Draft)
 *
 * Permission is hereby granted to any individual or legal entity obtaining a copy
 * of this licensed work (including the source code, documentation and/or related
 * items, hereinafter collectively referred to as the "licensed work"), free of
 * charge, to deal with the licensed work for any purpose, including without
 * limitation, the rights to use, reproduce, modify, prepare derivative works of,
 * publish, distribute and sublicense the licensed work, subject to the following
 * conditions:
 *
 * 1.  The individual or the legal entity must conspicuously display, without
 *     modification, this License on each redistributed or derivative copy of the
 *     Licensed Work.
 *
 * 2.  The individual or the legal entity must strictly comply with all applicable
 *     laws, regulations, rules and standards of the jurisdiction relating to
 *     labor and employment where the individual is physically located or where
 *     the individual was born or naturalized; or where the legal entity is
 *     registered or is operating (whichever is stricter). In case that the
 *     jurisdiction has no such laws, regulations, rules and standards or its
 *     laws, regulations, rules and standards are unenforceable, the individual
 *     or the legal entity are required to comply with Core International Labor
 *     Standards.
 *
 * 3.  The individual or the legal entity shall not induce or force its
 *     employee(s), whether full-time or part-time, or its independent
 *     contractor(s), in any methods, to agree in oral or written form,
 *     to directly or indirectly restrict, weaken or relinquish his or
 *     her rights or remedies under such laws, regulations, rules and
 *     standards relating to labor and employment as mentioned above,
 *     no matter whether such written or oral agreement are enforceable
 *     under the laws of the said jurisdiction, nor shall such individual
 *     or the legal entity limit, in any methods, the rights of its employee(s)
 *     or independent contractor(s) from reporting or complaining to the copyright
 *     holder or relevant authorities monitoring the compliance of the license
 *     about its violation(s) of the said license.
 *
 * THE LICENSED WORK IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN ANY WAY CONNECTION
 * WITH THE LICENSED WORK OR THE USE OR OTHER DEALINGS IN THE LICENSED WORK.
 */

package job

import (
	"github.com/hashicorp/nomad/e2e/e2eutil"
	"github.com/hashicorp/nomad/e2e/framework"
	"github.com/stretchr/testify/require"
)

type BasicJobTest struct {
	framework.TC
	jobIds []string
}

func init() {
	framework.AddSuites(&framework.TestSuite{
		Component:   "BasicJobTestSuite",
		CanRunLocal: true,
		Parallel:    true,
		Cases: []framework.TestCase{
			new(BasicJobTest),
		},
	})
}

func (tc *BasicJobTest) BeforeAll(f *framework.F) {
	// Ensure cluster has leader before running tests
	e2eutil.WaitForLeader(f.T(), tc.Nomad())
	// Ensure that we have four client nodes in ready state
	e2eutil.WaitForNodesReady(f.T(), tc.Nomad(), 1)
}

// 一个端到端的测试，会涉及不同的功能模块协作，模块间的组合，涉及其中的API，是不能穷尽的，
// 应当测试用户有意义的使用流程，同时最好有API的覆盖率作为次要指标；提高API的覆盖率应当是单元测试的主要工作
func (tc *BasicJobTest) TestRawExecJob(f *framework.F) {
	// Get Nomad api.Client
	nomadClient := tc.Nomad()

	require := require.New(f.T())

	// Listing jobs before registering returns nothing
	jobs := nomadClient.Jobs()
	resp, _, err := jobs.List(nil)
	require.Nil(err)
	require.Emptyf(resp, "expected 0 jobs, got: %d", len(resp))

	jobId := "testRawExecJobId"
	tc.jobIds = append(tc.jobIds, jobId)

	// User commit a job wait nomad cluster handle
	// If everything is ok, get the allocations
	allocs := e2eutil.RegisterAndWaitForAllocs(f.T(), nomadClient, "job/input/raw_exec.nomad", tc.jobIds[0])
	require.Len(allocs, 1)

	// List jobs
	resp, _, _ = jobs.List(nil)
	require.Len(resp, 1)
	versions, _, _, err := jobs.Versions(tc.jobIds[0], false, nil)
	require.NoError(err)
	require.True(len(versions) == 1)

	// Update the job
	e2eutil.RegisterAndWaitForAllocs(f.T(), nomadClient, "job/input/raw_exec.nomad.v2", tc.jobIds[0])
	resp, _, _ = jobs.List(nil)
	require.Len(resp, 1)
	versions, _, _, err = jobs.Versions(tc.jobIds[0], false, nil)
	require.NoError(err)
	require.True(len(versions) == 2)

	// Delete the job
	jobs.Deregister(tc.jobIds[0], true, nil)
	resp, _, _ = jobs.List(nil)
	require.Len(resp, 0)
}

func (tc *BasicJobTest) AfterEach(f *framework.F) {
	nomadClient := tc.Nomad()
	jobs := nomadClient.Jobs()
	// Stop all jobs in test
	for _, id := range tc.jobIds {
		jobs.Deregister(id, true, nil)
	}
	// Garbage collect
	nomadClient.System().GarbageCollect()
}
