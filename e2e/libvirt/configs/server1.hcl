log_level = "DEBUG"
data_dir = "/tmp/server1"

# Enable the server
server{
  enabled = true
  bootstrap_expect = 1
}

consul {
  address = "127.0.0.1:8500"
}

disable_update_check = true