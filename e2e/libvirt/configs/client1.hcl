log_level = "DEBUG"
data_dir = "/tmp/client1"
plugin_dir = "/opt/nomad/data/plugins" # should put `nomad-driver-libvirt` binary before test

# Enable the client
client {
  enabled = true
  server_join {
      retry_join = ["127.0.0.1:4647"]
    }
  options {
    "driver.exec" = "1"
    "driver.raw_exec.enable" = "1"
    "docker.privileged.enabled" = "true"
  }
}

consul {
  address = "127.0.0.1:8500"
}

ports{
  http = 5656
}

disable_update_check = true
