#!/usr/bin/env bash

# This script takes path to a binary and runs a 1 server, 1 node (with `nomad-driver-libvirt` binary in specific `plugin_dir`) cluster
if [ "$#" -ne 1 ]; then
    echo "expected usage ./run_cluster.sh /path/to/nomad/binary"
    exit 255
fi
NOMAD_BINARY=$1

# clean
pkill consul
pkill nomad
rm -rf /tmp/{server1,client1}

# launch consul
(consul agent -dev)&

# launch server 
( ${NOMAD_BINARY} agent -config=configs/server1.hcl 2>&1 | tee "/tmp/server1.log" ; echo "Exit code: $?" >> "/tmp/server1.log" ) &

# launch client 1
( ${NOMAD_BINARY} agent -config=configs/client1.hcl 2>&1 | tee "/tmp/client1.log" ; echo "Exit code: $?" >> "/tmp/client1.log" ) &

# see nomad and consul process
pgrep consul -a && pgrep nomad -a
