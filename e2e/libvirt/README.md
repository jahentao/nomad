Libvirt Driver Tests
=============

This directory has a set of scripts for Nomad libvirt driver e2e testing. The cluster's configs
are also included here. For ease of testing, the configs and node names use names like
`"server1"` and `"client1"` and the data directories are in `"/tmp/server1"`, `"/tmp/client1"`, etc

Before test, `nomad-driver-libvirt` binary should be put into `plugin_dir` specified in `configs/clinet1.hcl`

Start a cluster
===============
Use `run_cluster.sh` to start a 1 server, 1 client cluster locally.
The script takes the path to the Nomad binary. For example, to run a
0.9.0 Nomad cluster do:

```bash
$ sudo sh ./run_cluster.sh /path/to/Nomad0.9.0
```
Run cluster also assumes that `consul` exists in your path and runs a dev agent

Kill all nodes
==============

Just run:
```bash
$ pgrep consul -a && pgrep nomad -a
$ sudo pkill consul && sudo pkill nomad
```

