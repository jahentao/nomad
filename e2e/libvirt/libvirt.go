package libvirt

import (
	"github.com/hashicorp/nomad/e2e/framework"
	"github.com/stretchr/testify/require"
	"os/exec"
	"regexp"
	"strings"
	"testing"

	"github.com/hashicorp/nomad/e2e/e2eutil"
	"github.com/hashicorp/nomad/helper/uuid"
)

type LibvirtDriverTest struct {
	framework.TC
	jobIds []string
}

func init() {
	framework.AddSuites(&framework.TestSuite{
		Component:   "Libvirt",
		CanRunLocal: true,
		Cases: []framework.TestCase{
			new(LibvirtDriverTest),
		},
	})
}

func (tc *LibvirtDriverTest) BeforeAll(f *framework.F) {
	// TODO Configure the libvirt plugin dir

	// TODO for recover task, the noamd server should enable task data persistence

	// Ensure cluster has leader before running tests
	e2eutil.WaitForLeader(f.T(), tc.Nomad())
	// Ensure that we have 1 client nodes in ready state
	e2eutil.WaitForNodesReady(f.T(), tc.Nomad(), 1)
}

func requireLibvirt(t *testing.T) {
	bin := "virsh"
	outBytes, err := exec.Command(bin, "--version").Output()
	if err != nil {
		t.Skip("skipping, lxc not present")
	}
	out := strings.TrimSpace(string(outBytes))

	libvirtVersionRegex := regexp.MustCompile(`\d+\.\d+\.\d+`)
	matches := libvirtVersionRegex.FindStringSubmatch(out)
	if len(matches) != 1 {
		t.Skip("skipping, lxc not present")
	}
}

// TestLibvirtDriver_Start_Stop_Destroy_Task is an end to end test
// for controlling task's `start > stop > destroy` lifecycle.
// This runs a libvirt job.
func (tc *LibvirtDriverTest) TestStart_Stop_Destroy_Task(f *framework.F) {

	// require libvirt installed
	requireLibvirt(f.T())

	nomadClient := tc.Nomad()
	uuid := uuid.Generate()
	jobId := "libvirtjob" + uuid[0:8]
	tc.jobIds = append(tc.jobIds, jobId)

	// 1. Simulate user input a job configuration file `vim libvirt.nomad`,
	// then run the job `nomad run libvirt.nomad`
	allocs := e2eutil.RegisterAndWaitForAllocs(f.T(), nomadClient, "libvirt/input/libvirt.nomad", jobId)

	require := require.New(f.T())
	// After job is scheduled, an alloc will got after placement
	require.Len(allocs, 1)

	// Wait till alloc is running,
	// this step make sure `alloc.ClientStatus == structs.AllocClientStatusRunning`
	allocID := allocs[0].ID
	e2eutil.WaitForAllocRunning(f.T(), nomadClient, allocID)

	// Assert the job is running
	jobs := nomadClient.Jobs()
	job, _, _ := jobs.Info(jobId, nil)
	require.Equal("running", *job.Status, "job's status is not running")

	// 2. Simulate user stop a running job , like run `nomad stop ${jobName}`
	// In this step, the job will be stopped and then be destroyed
	_, _, e := jobs.Deregister(jobId, false, nil)
	require.NoError(e)

	// Wait till alloc is complete,
	e2eutil.WaitForAllocComplete(f.T(), nomadClient, allocID)

	// Assert the job is dead(stopped)
	job, _, _ = jobs.Info(jobId, nil)
	require.Equal("dead", *job.Status, "job's status is not dead")
}

func (tc *LibvirtDriverTest) AfterEach(f *framework.F) {
	nomadClient := tc.Nomad()
	jobs := nomadClient.Jobs()
	// Stop all jobs in test
	for _, id := range tc.jobIds {
		jobs.Deregister(id, true, nil)
	}
	// Garbage collect
	nomadClient.System().GarbageCollect()
}
