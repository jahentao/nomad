job "test_libvirt" {
  datacenters = ["dc1"]
  type        = "batch"

  group "test" {
   count = 1

    task "test1" {
      driver = "libvirt"

      config {
        name = "centos_vm_task"
        memory {
          value = 1
          unit = "Gib"
        }
        vcpu = 1
        disks = [
          {
            device = "disk"
            type = "file"
            source = "/var/lib/libvirt/images/centos7.0.qcow2" # should be prepared before testing
            target_bus = "virtio"
          }
        ]
        machine = "q35"
        interfaces = [
          {
            name = "eth0"
            model = "virtio"
            interface_binding_method = "Network"
            source_name = "default"
          }
        ]
      }
    }
  }
}
